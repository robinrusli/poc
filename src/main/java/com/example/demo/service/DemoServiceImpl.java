package com.example.demo.service;

import java.util.Set;
import java.util.Date;
import java.util.List;
import java.io.Reader;
import org.slf4j.Logger;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedWriter;
import org.slf4j.LoggerFactory;
import java.text.ParseException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import javax.validation.Validator;
import javax.validation.Validation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import javax.validation.ValidatorFactory;
import com.example.demo.dto.Uploadresult;
import com.example.demo.entity.Registration;
import javax.validation.ConstraintViolation;
import org.springframework.stereotype.Service;
import com.example.demo.constant.DemoConstant;
import org.springframework.web.multipart.MultipartFile;
import com.example.demo.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class DemoServiceImpl implements DemoService {
	
	private static final Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);
	
	@Autowired
	RegistrationRepository registrationRepo;
	
	private static Validator validator;
	
	static {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}
	
	@Override
	public List<Uploadresult> reader(MultipartFile uploadFile) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");		
		List<Uploadresult> result = new ArrayList<Uploadresult>();

		final Path tempFile = Files.createTempFile("temp", ".csv");
		tempFile.toFile().deleteOnExit();
		try(FileOutputStream out = new FileOutputStream(tempFile.toFile())) {
			IOUtils.copy(uploadFile.getInputStream(), out);
		}
		
		Reader reader = Files.newBufferedReader(tempFile);
		@SuppressWarnings("resource")
		CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);

		for(CSVRecord csvRecord : csvParser) {
			String nip = csvRecord.get(0);
			String name = csvRecord.get(1);
			String dobStr = csvRecord.get(2);
			String gender = csvRecord.get(3);
			String address = csvRecord.get(4);
			String idNumber = csvRecord.get(5);
			String phoneNumber = csvRecord.get(6);
			Date dob = null;
			
			try {
				dob = sdf.parse(dobStr);
			} catch(ParseException e) {
				dob = new Date();
			}
			
			List<String> errorMessages = new ArrayList<String>();
			Registration registration = new Registration();
			registration.setNip(nip);
			registration.setName(name);
			registration.setDob(dob);
			registration.setGender(gender.toUpperCase());
			registration.setAddress(address);
			registration.setIdNumber(idNumber);
			registration.setPhoneNumber(phoneNumber);
			
			Set<ConstraintViolation<Registration>> violations = validator.validate(registration);
			for(ConstraintViolation<Registration> violation: violations) {
				errorMessages.add(violation.getMessage());
			}
			
			Uploadresult uploadresult = new Uploadresult(registration);

			
			// All is good but NIP not checked uniqueness
			if(errorMessages.isEmpty()) {
				// Checking NIP uniqueness
				Registration checkNip = registrationRepo.findByNip(registration.getNip());
				
				if(checkNip != null) errorMessages.add("NIP " + registration.getNip() + " is already registered");
			}
			

			// All validation is done
			if(errorMessages.isEmpty()) {
				// Save data
				try {
					registration = registrationRepo.save(registration);
					uploadresult.setId(registration.getId());
					uploadresult.setNotes("OK");
					result.add(uploadresult);
				} catch(Exception e) {
					uploadresult.setNotes(e.getMessage());
					result.add(uploadresult);
				}
			} else {
				uploadresult.setNotes(String.join(", ", errorMessages));
				result.add(uploadresult);
			}
		}
		
		// Greeting
		doGreetAndWriteValidatedData(result, sdf);
		
		return result;
	}

	private void doGreetAndWriteValidatedData(List<Uploadresult> results, SimpleDateFormat sdf) throws IOException{
		for(Uploadresult result: results) {
			if(result.getNotes().equals("OK")) {
				logger.info("Sending message to " + (result.getGender().equals("M") ? "Pak " : "Bu ") + result.getName());
				
				FileWriter myWriter = new FileWriter(DemoConstant.SAVE_LOCATION + result.getNip() + ".txt");
				StringBuffer strData = new StringBuffer();
				strData.append(result.getName()).append("\t");
				strData.append(sdf.format(result.getDob())).append("\t");
				strData.append(result.getGender()).append("\t");
				strData.append(result.getAddress()).append("\t");
				strData.append(result.getIdNumber()).append("\t");
				strData.append(result.getPhoneNumber());
				
				myWriter.write(strData.toString());
				myWriter.close();
			}
		}
	}
	
	@Override
	public Path writer() throws IOException {
		List<Registration> result = (List<Registration>) registrationRepo.findAll();

		final Path tempFile = Files.createTempFile("temp", ".csv");
		tempFile.toFile().deleteOnExit();
		BufferedWriter writer = Files.newBufferedWriter(tempFile);

		@SuppressWarnings("resource")
		CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                .withHeader("ID", "NIP", "Name", "Date of Birth", "Gender", "Address", "ID Number", "Phone Number"));		

		for(Registration registration: result) {
			csvPrinter.printRecord(registration.getId(), registration.getNip(), registration.getName(), registration.getDob(), registration.getGender(), registration.getAddress(), registration.getIdNumber(), registration.getPhoneNumber());
		}
		csvPrinter.flush();
		
		return tempFile;
	}
}
