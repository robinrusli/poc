package com.example.demo.service;

import java.util.List;
import java.nio.file.Path;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.Uploadresult;

public interface DemoService {

	List<Uploadresult> reader(MultipartFile uploadFile) throws IOException;
	Path writer() throws IOException;
}
