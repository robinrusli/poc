package com.example.demo.validator;

import java.util.Date;
import java.text.SimpleDateFormat;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DobValidator implements ConstraintValidator<DobConstraint, Date> {

	private static SimpleDateFormat sdf;
	
	static {
		sdf = new SimpleDateFormat("yyyyMMdd");		
	}
	
    @Override
    public void initialize(DobConstraint gender) {
    }

    @Override
    public boolean isValid(Date dob, ConstraintValidatorContext cxt) {
    	Date now = new Date();
    	
    	Integer dateNum = Integer.valueOf(sdf.format(now));
    	Integer dobNum = Integer.valueOf(sdf.format(dob));
    	
        return dateNum > dobNum;
    }
}
