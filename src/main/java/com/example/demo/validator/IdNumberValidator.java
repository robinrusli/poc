package com.example.demo.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IdNumberValidator implements ConstraintValidator<IdNumberConstraint, String> {

    @Override
    public void initialize(IdNumberConstraint idNumber) {
    }

    @Override
    public boolean isValid(String idNumber, ConstraintValidatorContext cxt) {
    	Pattern p = Pattern.compile("[a-zA-Z_]+", Pattern.CASE_INSENSITIVE);    	
    	Matcher m = p.matcher(idNumber);
    	
        return m.find();
    }
}
