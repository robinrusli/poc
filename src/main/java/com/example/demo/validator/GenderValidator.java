package com.example.demo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GenderValidator implements ConstraintValidator<GenderConstraint, String> {
	
    @Override
    public void initialize(GenderConstraint gender) {
    }

    @Override
    public boolean isValid(String gender, ConstraintValidatorContext cxt) {
        return gender.equalsIgnoreCase("M") || gender.equalsIgnoreCase("F");
    }
}
