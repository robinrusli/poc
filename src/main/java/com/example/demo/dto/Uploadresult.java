package com.example.demo.dto;

import com.example.demo.entity.Registration;

public class Uploadresult extends Registration {
	
	private String notes;

	public Uploadresult() {

	}

	public Uploadresult(Registration registration) {
		super.setId(registration.getId());
		super.setNip(registration.getNip());
		super.setName(registration.getName());
		super.setDob(registration.getDob());
		super.setGender(registration.getGender());
		super.setAddress(registration.getAddress());
		super.setIdNumber(registration.getIdNumber());
		super.setPhoneNumber(registration.getPhoneNumber());
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
