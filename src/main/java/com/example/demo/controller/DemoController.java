package com.example.demo.controller;

import java.util.List;
import java.nio.file.Path;
import java.io.IOException;
import java.nio.file.Files;
import com.example.demo.dto.Uploadresult;
import org.springframework.http.MediaType;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import com.example.demo.service.DemoService;
import org.springframework.http.ResponseEntity;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;
import com.example.demo.repository.RegistrationRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class DemoController {

	@Autowired
	private DemoService demoService;
	
	@Autowired
	RegistrationRepository registrationRepo;

	@GetMapping
	@RequestMapping(value = "/writer")
	public ResponseEntity<Resource> writer() {
		Path file = null;
		ByteArrayResource resource = null;
		HttpHeaders header = new HttpHeaders();
		header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=output.csv");

		try {
			file = demoService.writer();
			resource = new ByteArrayResource(Files.readAllBytes(file));
		} catch (IOException e) {
			return ResponseEntity.badRequest().build();
		}
		
		return ResponseEntity.ok()
			.headers(header)
			.contentType(MediaType.parseMediaType("text/csv"))
			.body(resource);
	}
	
	@PostMapping
	@RequestMapping(value = "/reader")
	public ResponseEntity<List<Uploadresult>> reader(@RequestParam(value = "file") MultipartFile uploadFile) {
		List<Uploadresult> result = null;

		try {
			result = demoService.reader(uploadFile);
		} catch(IOException e) {
			return ResponseEntity.badRequest().build();
		}
		
		return ResponseEntity.ok()
			.contentType(MediaType.parseMediaType("application/json"))
			.body(result);
	}
}
