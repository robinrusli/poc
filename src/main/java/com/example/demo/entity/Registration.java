package com.example.demo.entity;

import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotBlank;
import com.example.demo.validator.DobConstraint;
import com.example.demo.validator.NameConstraint;
import com.example.demo.validator.PhoneConstraint;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.example.demo.validator.GenderConstraint;
import com.example.demo.validator.IdNumberConstraint;

@Entity
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true)
    @NotBlank(message = "NIP is mandatory")
    private String nip;
    
    @Column
    @NotBlank(message = "Name is mandatory")
    @NameConstraint
    private String name;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone="Asia/Jakarta")
    @DobConstraint
    private Date dob;
    
    @Column
    @NotBlank(message = "Gender is mandatory")
    @GenderConstraint
    private String gender;
    
    @Column
    @Size(min = 20, message = "Address at least 20 characters")
    private String address;
	
    @Column
    @NotBlank(message = "ID Number is mandatory")
    @IdNumberConstraint
    private String idNumber;
    
    @Column
    @NotBlank(message = "Phone Number is mandatory")
    @PhoneConstraint
    private String phoneNumber;

	public Registration() {

	}

	public Registration(long id, @NotBlank(message = "NIP is mandatory") String nip,
			@NotBlank(message = "Name is mandatory") String name,
			@NotBlank(message = "Date of birth is mandatory") Date dob,
			@NotBlank(message = "Gender is mandatory") String gender,
			@Size(min = 20, message = "Address at least 20 characters") String address,
			@NotBlank(message = "ID Number is mandatory") String idNumber,
			@NotBlank(message = "Phone Number is mandatory") String phoneNumber) {
		super();
		this.id = id;
		this.nip = nip;
		this.name = name;
		this.dob = dob;
		this.gender = gender;
		this.address = address;
		this.idNumber = idNumber;
		this.phoneNumber = phoneNumber;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Registration [id=" + id + ", nip=" + nip + ", name=" + name + ", dob=" + dob + ", gender=" + gender
				+ ", address=" + address + ", idNumber=" + idNumber + ", phoneNumber=" + phoneNumber + "]";
	}
}
