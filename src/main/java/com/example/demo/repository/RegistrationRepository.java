package com.example.demo.repository;

import com.example.demo.entity.Registration;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface RegistrationRepository extends CrudRepository<Registration, Long> {

	Registration findByNip(String nip);
}
